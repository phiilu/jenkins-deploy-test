const express = require("express");

const app = express();

app.get("/", (req, res) =>
  res.json({ message: "Hello World", timestamp: new Date() })
);

app.listen(3000, () => console.log("Server listening on Port 3000"));
