#!/bin/sh      
ssh florian@192.168.86.146 <<EOF        
 cd /opt/parma-ui  
 git pull       
 npm install --production       
 pm2 start index.js --name="parma-ui"
 exit       
EOF